<?php

/**
 * @Author Diveev Alexey
 * @email  alexz29@yandex.ru
 * Date: 24.08.14
 * Time: 17:10
 *
 * gen=generate
 */
class ArrayToYml
{
    /**
     * $array = array(
     * 0 => array(
     * 'category' => 'Книги',
     * 'attr' => array(
     * 'id' => 1,
     * 'parentId' => 0,
     * ),
     * ),
     * );
     *
     * @param $tag_array
     * @param string $str пустая строка в начале
     * @return string
     */
    protected function  genXML($tag_array = null, $str = "")
    {
        if (is_array($tag_array)) {
            $i = 0;
            foreach ($tag_array as $row) {
                if(empty($row['attr'])){
                    $str_attr = "";
                }else{
                    $str_attr = "";
                    foreach ($row['attr'] as $key_attr => $value_attr) {
                        $str_attr .= " $key_attr=$value_attr ";
                    }
                }

                unset($row['attr']);

                foreach ($row as $key => $value) {
                    $str .= "<$key $str_attr>$value</$key>";
                }
            }
        }

        return $str . "\n";
    }

    /**
     * XML-заголовок со ссылкой на файл описания формата
     */
    function header($version = '1.0', $encoding = 'windows-1251')
    {
        return self::genXML(null,
            "<?xml version='$version' encoding='$encoding'?><!DOCTYPE yml_catalog SYSTEM 'shops.dtd'>
            ");
    }

    /**
     * Генерация корневого каталога <yml_catalog>
     */
    function ymlCatalog($date = '2010-04-01 17:00', $shop)
    {
        $yml_catalog_array = array(
            0 => array(
                'yml_catalog' => $shop,
                'attr' => array(
                    'date' => $date,
                ),
            ),
        );

        return self::genXML($yml_catalog_array);
    }

    /**
     * Элемент <shop> содержит описание магазина и его товарных предложений.
     */
    function shop($name, $company, $url, $platform, $version, $agency, $email, $currencies = array(), $categories = array(), $cpa, $offers = array())
    {
        $value_shop_array = array(
            0 => array(
                'name' => $name,
                'attr' => array(),
            ),
            2 => array(
                'company' => $company,
                'attr' => array(),
            ),
            3 => array(
                'url' => $url,
                'attr' => array(),
            ),
            4 => array(
                'platform' => $platform,
                'attr' => array(),
            ),
            5 => array(
                'version' => $version,
                'attr' => array(),
            ),
            6 => array(
                'agency' => $agency,
                'attr' => array(),
            ),
            7 => array(
                'email' => $email,
                'attr' => array(),
            ),
            8 => array(
                'currencies' => $currencies,
                'attr' => array(),
            ),
            9 => array(
                'categories' => $categories,
                'attr' => array(),
            ),
            10 => array(
                'cpa' => $cpa,
                'attr' => array(),
            ),
            11 => array(
                'offers' => $offers,
                'attr' => array(),
            ),
        );

        $shop_array = array(
            0 => array(
                'shop' => self::genXML($value_shop_array),
            ),
        );

        return self::genXML($shop_array);
    }

    /**
     * Элемент <currencies> задает список курсов валют магазина.
     * Каждая из валют описывается отдельным элементом <currency>.
     */
    function currencies()
    {

    }

    /**
     * В элементе <categories> содержится список категорий магазина
     *
     * @param $id идентификатор вашей категории товаров
     * @param $parentId идентификатор более высокой по иерархии (родительской) категории товаров
     * @param $nameCategory наименование категории товаров
     */
    function categories($id, $parentId, $nameCategory)
    {

    }

    /**
     * В элементе <local_delivery_cost> указывается стоимость доставки для своего региона.
     */
    function localDeliveryCost()
    {

    }


    /**
     * <offers> содержится список товарных предложений магазинов
     * @param  $id идентификатор товарного предложения
     * @param $available bool
     */
    function offers($id, $available)
    {

    }

    /**
     * Для каждого товарного предложения в элементе <offer> можно указать до десяти URL-адресов изображений,
     * соответствующих данному товарному предложению.
     */
    function picture()
    {

    }

    /**
     * В элементе <description> указывается описание товарного предложения (длина не более 512 символов)
     */
    function description()
    {

    }

    /**
     * В элементе <name> указывается заголовок товарного предложения (длина не более 255 символов)
     */
    function name()
    {

    }

    /**
     * Возможность доставки товара обозначается элементом <delivery>.
     * Элемент описывает наличие доставки — значение true или отсутствие доставки — значение false
     */
    function delivery()
    {

    }

    /**
     * Наличие самовывоза описывается элементом <pickup>.
     * Если предусмотрена возможность предварительно заказать данный товар и забрать его в пункте выдачи заказов,
     * для элемента используется значение true. В противном случае — false.
     */
    function pickup()
    {

    }

    /**
     * Наличие точки продаж, где товар есть в наличии и его можно купить без предварительного заказа,
     * указывается значением true у элемента <store>. Если точки продаж нет, используется значение false.
     */
    function store()
    {

    }

    /**
     * Элемент <adult> обязателен для обозначения товара,
     * имеющего отношение к удовлетворению сексуальных потребностей,
     * либо иным образом эксплуатирующего интерес к сексу.
     */
    function adult()
    {

    }

    /**
     * Элемент <barcode> предназначен для передачи численных штрихкодов (баркодов) товара.
     */
    function barcode()
    {

    }

    /**
     * С помощью элемента <cpa> можно управлять как участием отдельных товарных предложений в программе
     * «Покупка на Маркете», так и участием всего YML-файла.
     *
     * Элемент может принимать следующие значения:
     * 0 — товар/YML-файл не участвует в «Покупке на Маркете»;
     * 1 — товар/YML-файл участвует в «Покупке на Маркете».
     */
    function cpa()
    {

    }

    /**
     * С помощью элемента <rec> задаются аксессуары,
     * рекомендуемые для покупки вместе с текущим товаром.
     * Параметр является необязательным.
     *
     * В <rec> указываются идентификаторы товарных предложений (атрибут id элемента <offer>)
     */
    function rec()
    {

    }

    /**
     * Элемент <param> предназначен для описания характеристик товара
     *
     * @param $name название параметра;
     * @param $unit единица измерения (для числовых параметров);
     * @param $value значение параметра.
     */
    function param($name, $unit, $value)
    {

    }


    /**
     * В элементе <vendor> указывается производитель товара или его торговая марка.
     */
    function vendor()
    {

    }

    /**
     * Параметр <oldprice> необходим для расчета скидки на товар.
     * В <oldprice> указывается старая цена товара,
     * которая должна быть обязательно выше новой цены (<price>)
     */
    function oldPrice()
    {

    }
}

/*
        $array = array(
            0 => array(
                'category' => 'Книги',
                'attr' => array(
                    'id' => 1,
                    'parentId' => 0,
                ),
            ),
        );*/

$yml = new ArrayToYml();
print_r($yml->header());
print_r($yml->ymlCatalog(
        '', $yml->shop('name', 'company', 'http://123.ru', 'CMS', '2.3', 'Agency', 'cms@cms.ru', '', '', 0, '')
    )
);